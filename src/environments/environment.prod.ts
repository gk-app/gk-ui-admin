import { gkConstants } from './gk-constants';

export const environment = {
  production: true,
  apiUrl: 'https://gkdata.herokuapp.com',
  constants: gkConstants
};
