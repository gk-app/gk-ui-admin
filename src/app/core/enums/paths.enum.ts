export enum PathsEnum {
  DASHBOARD = 'dashboard',
  PLAYERS = 'players',
  COURTS = 'courts'
}
