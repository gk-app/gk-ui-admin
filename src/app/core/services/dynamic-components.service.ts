import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  EmbeddedViewRef,
  Inject,
  Injectable,
  Injector,
  Type
} from '@angular/core';
import { PopupConfig } from "../../shared/dialogs/popup/popup.models";
import { DOCUMENT } from "@angular/common";

@Injectable({providedIn: 'root'})
export class DynamicComponentsService {

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private appRef: ApplicationRef,
              @Inject(DOCUMENT) private document: Document) {}

  appendPopup<T extends AutoClosable<T>>(component: Type<T>, config: PopupConfig, closeIn?: number) {
    // Create a component reference from the component
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
    const injector = Injector.create({providers: [{provide: PopupConfig, useValue: config}]});
    const componentRef = componentFactory.create(injector);

    // Inject into component a reference to itself
    componentRef.instance.componentRef = componentRef;

    // Attach component to the appRef so that it's inside the ng component tree
    this.appRef.attachView(componentRef.hostView);

    // Get DOM element from component
    const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;

    // Append DOM element to the body
    this.document.body.appendChild(domElem);

    // Remove component from the component tree and from the DOM
    if (closeIn) {
      setTimeout(() => {
        this.appRef.detachView(componentRef.hostView);
        componentRef.destroy();
      }, closeIn);
    }
  }
}

export interface AutoClosable<T> {
  componentRef: ComponentRef<T>;
  close: () => void;
}
