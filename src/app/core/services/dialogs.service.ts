import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { OkDialogComponent } from '../../shared/dialogs/ok-dialog/ok-dialog.component';
import { ConfirmDialogComponent } from '../../shared/dialogs/confirm-dialog/confirm-dialog.component';
import { ConfirmDialogData } from "../../shared/dialogs/confirm-dialog/confirm-dialog.models";
import { NotificationDialogData } from "../../shared/dialogs/ok-dialog/notification-dialog.models";

@Injectable({
  providedIn: 'root'
})
export class DialogsService {

  constructor(private dialog: MatDialog) { }

  openOkDialog(data: NotificationDialogData, styles: string | Array<string>): MatDialogRef<OkDialogComponent> {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = data;
    dialogConfig.panelClass = styles;

    return this.dialog.open(OkDialogComponent, dialogConfig);
  }

  openConfirmDialog(data: ConfirmDialogData, styles: string | Array<string>): MatDialogRef<ConfirmDialogComponent> {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = data;
    dialogConfig.panelClass = styles;

    return this.dialog.open(ConfirmDialogComponent, dialogConfig);
  }
}
