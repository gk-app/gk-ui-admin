import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { NGXLogger } from 'ngx-logger';

@Injectable({providedIn: 'root'})
export class RestClientService {

  constructor(private http: HttpClient,
              private logger: NGXLogger) { }


  postData<T>(url: string, body: T): Observable<T> {
    return this.http.post<T>(url, body)
      .pipe(
        tap(
          () => this.logger.info(`Data was successfully send at ${url}`),
          (error) => {
            this.logger.error(`Request to ${url} failed.`);
            this.logger.error(error);
          }
        )
      );
  }

  getData<T>(url: string): Observable<T> {
    return this.http.get<T>(url)
      .pipe(
        tap(
          () => this.logger.info(`Data was successfully retrieved from ${url}`),
          (error) => {
            this.logger.error(`Request to ${url} failed.`);
            this.logger.error(error);
          }
        )
      );
  }

  putData<T>(url: string, body: T): Observable<T> {
    return this.http.put<T>(url, body)
      .pipe(
        tap(
          () => this.logger.info(`Data was successfully send at ${url}`),
          (error) => {
            this.logger.error(`Request to ${url} failed.`);
            this.logger.error(error);
          }
        )
      );
  }

  deleteData<T>(url: string): Observable<T> {
    return this.http.delete<T>(url)
      .pipe(
        tap(
          () => this.logger.info(`Data was successfully send at ${url}`),
          (error) => {
            this.logger.error(`Request to ${url} failed.`);
            this.logger.error(error);
          }
        )
      );
  }
}
