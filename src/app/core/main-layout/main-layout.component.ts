import { Component } from '@angular/core';

@Component({
  selector: 'bem-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent {

  isSidenavPinned = true;

  onPinSidenav(isSidenavPinned: boolean): void {
    this.isSidenavPinned = isSidenavPinned;
  }
}
