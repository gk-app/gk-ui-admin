import { Component, EventEmitter, Output } from '@angular/core';
import { PathsEnum } from "../../enums/paths.enum";

@Component({
  selector: 'bem-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent {

  DASHBOARD = PathsEnum.DASHBOARD;

  @Output()
  pin = new EventEmitter<boolean>();

  isOpen = true;
  isPinned = true;
  isExpanded = true;

  expandSideNav() {
    if (this.isPinned) {
      return;
    } else {
      this.isExpanded = true;
    }
  }

  collapseSideNave() {
    if (this.isPinned) {
      return;
    } else {
      this.isExpanded = false;
    }
  }

  togglePin() {
    this.isPinned = !this.isPinned;
    this.pin.emit(this.isPinned);
  }
}
