import { NgModule } from '@angular/core';
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatToolbarModule } from "@angular/material/toolbar";
import { LoggerModule, NgxLoggerLevel } from "ngx-logger";
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { HeaderComponent } from './main-layout/header/header.component';
import { FooterComponent } from './main-layout/footer/footer.component';
import { SidenavComponent } from './main-layout/sidenav/sidenav.component';
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatListModule } from "@angular/material/list";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";


@NgModule({
  declarations: [MainLayoutComponent, HeaderComponent, FooterComponent, SidenavComponent],
  imports: [
    // angular
    BrowserModule,
    RouterModule,
    HttpClientModule,

    // utils
    BrowserAnimationsModule,
    LoggerModule.forRoot({
      level: NgxLoggerLevel.INFO
    }),

    // material
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule
  ],
  exports: [MainLayoutComponent]
})
export class CoreModule {}
