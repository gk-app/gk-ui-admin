import { Court } from './court';
import { PlayerExperience } from './enum/player-experience';
import { Team } from './team';
import { Award } from './award';

export interface Player {
  avatar: string;
  awards: Array<Award>;
  createdAt: string;
  dateOfBirth: string;
  email: string;
  experience: PlayerExperience;
  fname: string;
  isMale: boolean;
  lastSeen: string;
  lname: string;
  nickname: string;
  playerId: string;
  residence: Court;
  team: Team;
  thumbnail: string;
}
