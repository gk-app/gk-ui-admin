import { SystemFormat } from './enum/system-format';
import { Draw } from './enum/draw';

export interface Rules {
  attackTime: number;
  createdAt: string;
  description: string;
  drawInfo: Draw;
  name: string;
  rulesId: string;
  systemFormat: SystemFormat;
  teamFormat: number;
  teamsLimit: number;
  timeFormat: string;
}
