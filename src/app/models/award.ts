import { AwardCategory } from './enum/award-category';
import { Player } from './player';

export interface Award {
  avatar: string;
  awardCategory: AwardCategory;
  awardId: number;
  description: string;
  name: string;
  players: Array<Player>;
  thumbnail: string;
}
