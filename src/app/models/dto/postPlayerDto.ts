export class PostPlayerDto {
  fname: string = '';
  lname: string = '';
  email: string = '';
  dateOfBirth: string = '';
  nickname: string = '';
  isMale: boolean = true;
}
