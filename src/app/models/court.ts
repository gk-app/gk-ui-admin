import { Player } from './player';
import { CourtImage } from './court-image';

export interface Court {
  courtId: number;
  name: string;
  avatar: string;
  thumbnail: string;
  latitude: number;
  longitude: number;
  rating: number;
  description: string;
  createdAt: string;
  residents: Array<Player>;
  images: Array<CourtImage>;
}
