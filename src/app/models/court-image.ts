export interface CourtImage {
  courtImageId: number;
  link: string;
  thumbnail: string;
}
