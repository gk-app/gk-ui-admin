export interface News {
  avatar: string;
  content: string;
  createdAt: string;
  newsId: number;
  thumbnail: string;
  title: string;
}
