import { Player } from './player';
import { Court } from './court';
import { Rules } from './rules';

export interface Tournament {
  avatar: string;
  coorganizer: Player;
  createdAt: string;
  date: string;
  judgeOne: Player;
  judgeTwo: Player;
  name: string;
  organizer: Player;
  place: Court;
  thumbnail: string;
  tournamentId: number;
  rules: Rules;
}
