import { Player } from './player';

export interface Team {
  avatar: string;
  captain: Player;
  createdAt: string;
  name: string;
  players: Array<Player>;
  teamId: number;
  thumbnail: string;
}
