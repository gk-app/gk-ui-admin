export enum AwardCategory {
  TEAM = 'TEAM',
  PERSONAL = 'PERSONAL',
  COMMUNITY = 'COMMUNITY'
}
