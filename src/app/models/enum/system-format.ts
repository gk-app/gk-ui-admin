export enum SystemFormat {
  KNOCKOUT = 'KNOCKOUT',
  GROUP = 'GROUP',
  MIXED = 'MIXED'
}
