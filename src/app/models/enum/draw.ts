export enum Draw {
  MANUAL = 'MANUAL',
  AUTOMATED = 'AUTOMATED',
  MIXED = 'MIXED'
}
