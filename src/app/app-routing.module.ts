import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PathsEnum } from "./core/enums/paths.enum";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: PathsEnum.DASHBOARD
  },
  {
    path: PathsEnum.DASHBOARD,
    loadChildren: () => import('./features/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: PathsEnum.PLAYERS,
    loadChildren: () => import('./features/players/players.module').then(m => m.PlayersModule)
  },
  {
    path: PathsEnum.COURTS,
    loadChildren: () => import('./features/courts/courts-list/courts-list.module').then(m => m.CourtsListModule)
  },
  {
    path: `${PathsEnum.COURTS}/:id`,
    loadChildren: () => import('./features/courts/court-details/court-details.module').then(m => m.CourtDetailsModule)
  },
  {
    path: '**',
    redirectTo: PathsEnum.DASHBOARD
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
