import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogsModule } from "./dialogs/dialogs.module";
import { FlexLayoutModule } from "@angular/flex-layout";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { EntityListHeaderModule } from "./entity-list-header/entity-list-header.module";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DialogsModule,
    MatButtonModule,
    MatIconModule,
  ],
  exports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    EntityListHeaderModule,
  ]
})
export class SharedModule {}
