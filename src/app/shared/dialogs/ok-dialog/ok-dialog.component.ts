import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NotificationDialogData } from "./notification-dialog.models";

@Component({
  selector: 'bem-ok-dialog',
  templateUrl: './ok-dialog.component.html'
})
export class OkDialogComponent {
  data: NotificationDialogData;

  constructor(public dialogRef: MatDialogRef<OkDialogComponent>,
              @Inject(MAT_DIALOG_DATA) data: NotificationDialogData) {
    this.data = data;
  }

  close() {
    this.dialogRef.close();
  }
}
