export interface NotificationDialogData {
  title: string;
  description: string;
}
