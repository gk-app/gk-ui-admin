import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OkDialogComponent } from './ok-dialog/ok-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { PopupComponent } from './popup/popup.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    MatIconModule
  ],
  declarations: [
    OkDialogComponent,
    PopupComponent,
    ConfirmDialogComponent
  ]
})

export class DialogsModule {}
