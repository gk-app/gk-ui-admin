import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ConfirmDialogData } from "./confirm-dialog.models";

@Component({
  selector: 'bem-confirm-dialog',
  templateUrl: './confirm-dialog.component.html'
})
export class ConfirmDialogComponent {

  data: ConfirmDialogData

  constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>,
              @Inject(MAT_DIALOG_DATA) data: ConfirmDialogData) {
    this.data = data;
  }

  close(isConfirmed: boolean) {
    this.dialogRef.close(isConfirmed);
  }
}
