import { Component, ComponentRef, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AutoClosable } from "../../../core/services/dynamic-components.service";
import { PopupConfig } from "./popup.models";

@Component({
  selector: 'bem-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit, AutoClosable<PopupComponent> {
  @Input()
  componentRef!: ComponentRef<PopupComponent>;
  isRaised = false;

  constructor(private router: Router,
              public config: PopupConfig) {}

  ngOnInit(): void {
    this.isRaised = true;
  }

  navigate(): void {
    if (this.config.data.link) {
      this.router.navigate([this.config.data.link.path]);
      this.close();
    }
  }

  close(): void {
    this.componentRef.destroy();
  }
}
