export class PopupConfig {
  theme: string
  data: PopupData;

  constructor(theme: string, data: PopupData) {
    this.theme = theme;
    this.data = data;
  }
}

export interface PopupData {
  content: Array<string>;
  title?: string;
  link?: PopupLink;
}

export interface PopupLink {
  text: string;
  path: string;
}
