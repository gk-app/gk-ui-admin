import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntityListHeaderComponent } from './entity-list-header.component';
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { FlexLayoutModule } from "@angular/flex-layout";

@NgModule({
  declarations: [EntityListHeaderComponent],
  exports: [
    EntityListHeaderComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
  ]
})
export class EntityListHeaderModule {}
