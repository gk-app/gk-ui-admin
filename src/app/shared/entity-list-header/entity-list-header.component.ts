import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'bem-entity-list-header',
  templateUrl: './entity-list-header.component.html',
  styleUrls: ['./entity-list-header.component.scss']
})
export class EntityListHeaderComponent {

  @Input()
  entityName = ''

  @Output()
  clickCreateBtnEvent = new EventEmitter<void>();

  onClickCreateBtn() {
    this.clickCreateBtnEvent.emit();
  }
}
