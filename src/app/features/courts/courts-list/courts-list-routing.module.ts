import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourtsListComponent } from './courts-list.component';

const routes: Routes = [
  {
    path: '', component: CourtsListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourtsListRoutingModule {}
