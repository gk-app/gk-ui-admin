import { NgModule } from '@angular/core';
import { SharedModule } from "../../../shared/shared.module";

import { CourtsListRoutingModule } from './courts-list-routing.module';
import { CourtsListComponent } from './courts-list.component';

@NgModule({
  imports: [
    SharedModule,
    CourtsListRoutingModule
  ],
  declarations: [CourtsListComponent]
})
export class CourtsListModule {}
