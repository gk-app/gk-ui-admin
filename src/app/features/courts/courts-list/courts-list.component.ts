import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { RestClientService } from '../../../core/services/rest-client.service';
import { NGXLogger } from 'ngx-logger';
import { Court } from '../../../models/court';

@Component({
  selector: 'bem-courts-list',
  templateUrl: './courts-list.component.html',
  styleUrls: ['./courts-list.component.scss']
})
export class CourtsListComponent implements OnInit {

  courtsList: Array<Court> = [];

  private courtsListUrl = environment.apiUrl + environment.constants.REST_API_PREFIX + '/courts';

  constructor(private restClient: RestClientService,
              private logger: NGXLogger) {
  }

  ngOnInit() {
    this.getCourtsList();
  }

  getCourtsList(): void {
    this.restClient.getData<Array<Court>>(this.courtsListUrl)
      .subscribe(
        (data: Array<Court>) => {
          this.courtsList = data;
          this.logger.log(data);
        },

        (error => this.logger.error(error))
      );
  }
}
