import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourtDetailsComponent } from './court-details.component';

const routes: Routes = [
  {
    path: '', component: CourtDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourtDetailsRoutingModule {}
