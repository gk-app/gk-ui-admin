import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import { RestClientService } from '../../../core/services/rest-client.service';
import { NGXLogger } from 'ngx-logger';
import { Court } from '../../../models/court';

@Component({
  selector: 'bem-court-details',
  templateUrl: './court-details.component.html',
  styleUrls: ['./court-details.component.scss']
})
export class CourtDetailsComponent implements OnInit {

  // TODO refactor. Definite assignment operator does not guarantee that field might be accessed before initialisation
  court!: Court;

  private courtUrl = environment.apiUrl + environment.constants.REST_API_PREFIX + '/courts/';

  constructor(private route: ActivatedRoute,
              private restClient: RestClientService,
              private logger: NGXLogger) { }

  ngOnInit() {
    this.route.paramMap
      .subscribe((paramMap) => {
        const id = paramMap.get('id');
        if (id) {
          this.getCourt(id);
        } else {
          this.logger.error("Court id was not provided");
        }
      });

  }

  getCourt(courtId: string): void {
    this.restClient.getData<Court>(this.courtUrl + courtId)
      .subscribe(
        (data: Court) => {
          this.court = data;
        },

        (error => this.logger.error(error))
      );
  }
}
