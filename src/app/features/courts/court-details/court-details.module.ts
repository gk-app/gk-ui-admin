import { NgModule } from '@angular/core';
import { SharedModule } from "../../../shared/shared.module";

import { CourtDetailsRoutingModule } from './court-details-routing.module';
import { CourtDetailsComponent } from './court-details.component';

@NgModule({
  imports: [
    SharedModule,
    CourtDetailsRoutingModule
  ],
  declarations: [CourtDetailsComponent]
})
export class CourtDetailsModule {}
