import { NgModule } from '@angular/core';
import { SharedModule } from "../../shared/shared.module";

import { DashboardComponent } from './dashboard.component';
import { MatCardModule } from '@angular/material/card';
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [{path: '', component: DashboardComponent}];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    MatCardModule
  ],
  declarations: [DashboardComponent],
  exports: [RouterModule]
})
export class DashboardModule {}
