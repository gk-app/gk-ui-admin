import { NgModule } from '@angular/core';
import { SharedModule } from "../../../shared/shared.module";

import { PlayerDetailsComponent } from './player-details.component';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [{path: '', component: PlayerDetailsComponent}];

@NgModule({
  imports: [
    SharedModule,
    [RouterModule.forChild(routes)],
    MatCardModule,
    FormsModule
  ],
  declarations: [PlayerDetailsComponent],
  exports: [RouterModule]
})
export class PlayerDetailsModule {}
