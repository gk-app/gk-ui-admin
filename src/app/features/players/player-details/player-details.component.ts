import { Component, OnInit } from '@angular/core';
import { Player } from '../../../models/player';
import { NGXLogger } from 'ngx-logger';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogsService } from '../../../core/services/dialogs.service';
import { ConfirmDialogData } from "../../../shared/dialogs/confirm-dialog/confirm-dialog.models";
import { NotificationDialogData } from "../../../shared/dialogs/ok-dialog/notification-dialog.models";
import { DynamicComponentsService } from "../../../core/services/dynamic-components.service";
import { PopupConfig } from "../../../shared/dialogs/popup/popup.models";
import { PopupThemesEnum } from "../../../shared/dialogs/popup/popup.enums";
import { PopupComponent } from "../../../shared/dialogs/popup/popup.component";
import { PlayersService } from "../players.service";

@Component({
  selector: 'bem-player-details',
  templateUrl: './player-details.component.html',
  styleUrls: ['./player-details.component.scss']
})
export class PlayerDetailsComponent implements OnInit {

  // TODO replace definite assignment with extended undefined type assertion when #121 is to be implemented
  player!: Player;
  isEditMode = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private playersService: PlayersService,
              private logger: NGXLogger,
              private dialogService: DialogsService,
              private dynamicComponentsService: DynamicComponentsService) { }

  ngOnInit(): void {
    this.getPlayer(this.route.snapshot.paramMap.get('id'));
  }

  savePlayer(): void {
    this.playersService.savePlayer(this.player)
      .subscribe(
        (data: Player) => {
          this.player = data;
          this.isEditMode = false;
          this.showSuccessPopup();
        },
        (error => {
          this.showErrorDialog();
          this.logger.error(`Error during saving Player with id ${this.player.playerId}: ${error}`);
        })
      );
  }

  cancelEditing(): void {
    this.isEditMode = false;
    this.getPlayer(this.player.playerId.toString());
  }

  deletePlayer(): void {
    const dialogData: ConfirmDialogData = {
      title: 'Player deletion',
      description: `Are you sure you want delete ${this.player.fname} ${this.player.lname}?`
    }
    const matDialogRef = this.dialogService.openConfirmDialog(dialogData, 'bem-warn-dialog');

    matDialogRef.afterClosed()
      .subscribe(
      isConfirmed => {
        if (isConfirmed) {
          this.playersService.deletePlayer(this.player.playerId)
            .subscribe(
              () => {
                this.logger.info(`Player with id: ${this.player.playerId} was successfully deleted`);

                const popupConfig = new PopupConfig(PopupThemesEnum.SUCCESS,
                  {
                    title: 'Player removed',
                    content: [`Player <strong>${this.player.fname} ${this.player.lname}</strong> was successfully deleted.`]
                  });
                this.dynamicComponentsService.appendPopup(PopupComponent, popupConfig, 7000);

                this.router.navigate(['/players/list']);
              },

              (error => {
                const data: NotificationDialogData = {
                  title: 'Error',
                  description: 'Error during player deleting'
                }

                this.dialogService.openOkDialog(data, 'bem-error-dialog');

                this.logger.error(`Error during deleting Player with id: ${this.player.playerId}: ${error}`);
              })
            );
        }
      }
    ) ;
  }

  setEditMode(): void {
    this.isEditMode = true;
    document.body.scrollIntoView({block: 'start', behavior: 'smooth'});
  }

  private getPlayer(playerId: string | null): void {
    if (playerId) {
      this.playersService.getPlayer(playerId)
        .subscribe(
          (data: Player) => this.player = data,
          (error => this.logger.error(error))
        );
    } else {
      this.logger.error("Player id was not provided");
    }
  }

  private showSuccessPopup(): void {
    const popupConfig = new PopupConfig(PopupThemesEnum.SUCCESS,
      {
        title: 'Player updated',
        content: [`Player <strong>${this.player.fname} ${this.player.lname}</strong> was successfully updated.`]
      });
    this.dynamicComponentsService.appendPopup(PopupComponent, popupConfig, 7000);
  }

  private showErrorDialog(): void {
    const data: NotificationDialogData = {
      title: 'Saving error',
      description: 'Error during saving Player\'s info'
    }

    this.dialogService.openOkDialog(data, 'bem-error-dialog');
  }
}
