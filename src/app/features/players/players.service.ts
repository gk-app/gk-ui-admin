import { Injectable } from '@angular/core';
import { RestClientService } from "../../core/services/rest-client.service";
import { Observable } from "rxjs";
import { Player } from "../../models/player";
import { environment } from "../../../environments/environment";
import { PostPlayerDto } from "../../models/dto/postPlayerDto";

@Injectable()
export class PlayersService {

  private playersUrl = environment.apiUrl + environment.constants.REST_API_PREFIX + '/players';

  constructor(private restClient: RestClientService,) { }

  getPlayerList(): Observable<Array<Player>> {
    return this.restClient.getData<Array<Player>>(this.playersUrl);
  }

  createPlayer(playerToSave: PostPlayerDto): Observable<PostPlayerDto> {
    return this.restClient.postData<PostPlayerDto>(this.playersUrl, playerToSave);
  }

  getPlayer(id: string): Observable<Player> {
    return this.restClient.getData<Player>(`${this.playersUrl}/${id}`);
  }

  savePlayer(player: Player): Observable<Player> {
    return this.restClient.putData<Player>(`${this.playersUrl}/${player.playerId}`, player);
  }

  deletePlayer(id: string): Observable<Array<Player>> {
    return this.restClient.deleteData<Array<Player>>(`${this.playersUrl}/${id}`);
  }
}
