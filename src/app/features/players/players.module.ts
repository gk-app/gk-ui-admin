import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { PlayersService } from "./players.service";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'list'
  },
  {
    path: 'list',
    loadChildren: () => import('./players-list/players-list.module').then(m => m.PlayersListModule)
  },
  {
    path: ':id',
    loadChildren: () => import('./player-details/player-details.module').then(m => m.PlayerDetailsModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [PlayersService]
})
export class PlayersModule { }
