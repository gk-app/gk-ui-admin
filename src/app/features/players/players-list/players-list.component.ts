import { Component, OnInit } from '@angular/core';
import { Player } from '../../../models/player';
import { NGXLogger } from 'ngx-logger';
import { MatDialog, MatDialogConfig, MatDialogRef } from "@angular/material/dialog";
import { PlayerFormDialogComponent } from "./player-form-dialog/player-form-dialog.component";
import { DynamicComponentsService } from "../../../core/services/dynamic-components.service";
import { PopupComponent } from "../../../shared/dialogs/popup/popup.component";
import { PopupConfig, PopupData } from "../../../shared/dialogs/popup/popup.models";
import { DialogsService } from "../../../core/services/dialogs.service";
import { ConfirmDialogData } from "../../../shared/dialogs/confirm-dialog/confirm-dialog.models";
import { PopupThemesEnum } from "../../../shared/dialogs/popup/popup.enums";
import { PlayersService } from "../players.service";
import { PathsEnum } from "../../../core/enums/paths.enum";

@Component({
  selector: 'bem-players-list',
  templateUrl: './players-list.component.html',
  styleUrls: ['./players-list.component.scss']
})
export class PlayersListComponent implements OnInit {

  playersList: Array<Player> = [];

  constructor(private playersService: PlayersService,
              private logger: NGXLogger,
              private matDialog: MatDialog,
              private dialogService: DialogsService,
              private dynamicComponentsService: DynamicComponentsService) { }

  ngOnInit() {
    this.getPlayerList();
  }

  createPlayer() {
    this.openFormDialog()
      .afterClosed()
      .subscribe((createdPlayer: Player) => {
          if (createdPlayer) {
            this.playersList.unshift(createdPlayer);
            this.showPlayerCreationPopup(createdPlayer);
          }
        }
      );
  }

  deletePlayer(id: string, fname: string, lname: string) {
    const dialogData: ConfirmDialogData = {
      title: 'Player deletion',
      description: `Are you sure you want delete ${fname} ${lname}?`
    }

    this.dialogService.openConfirmDialog(dialogData, 'bem-warn-dialog')
      .afterClosed()
      .subscribe(isConfirmed => {
          if (isConfirmed) {
            this.playersService
              .deletePlayer(id)
              .subscribe(() => {
                  this.logger.info(`Player with id: ${id} was successfully deleted`);
                  this.playersList = this.playersList.filter((player) => player.playerId !== id);
                  this.showPlayerRemovalPopup(fname, lname);
                },
                (error => {
                  this.logger.error(`Error during deleting Player with id: ${id}: ${error}`);
                  this.showPlayerRemovalErrorPopup(fname, lname);
                })
              );
          }
        }
      );
  }

  private getPlayerList(): void {
    this.playersService.getPlayerList()
      .subscribe(
        (data: Array<Player>) => this.playersList = data,
        (error => this.logger.error(error))
      );
  }

  private openFormDialog(): MatDialogRef<PlayerFormDialogComponent> {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.panelClass = 'bem-form-dialog';

    return this.matDialog.open(PlayerFormDialogComponent, dialogConfig);
  }

  private showPlayerCreationPopup(newPlayer: Player): void {
    const popupData: PopupData = {
      title: 'Player created',
      content: [`Player <strong>${newPlayer.fname} ${newPlayer.lname}</strong> was successfully created`],
      link: {
        text: 'Go to player details',
        path: `/${PathsEnum.PLAYERS}/${newPlayer.playerId}`
      }
    };
    const popupConfig = new PopupConfig(PopupThemesEnum.SUCCESS, popupData);
    this.dynamicComponentsService.appendPopup(PopupComponent, popupConfig, 7000);
  }

  private showPlayerRemovalPopup(fname: string, lname: string): void {
    const popupData: PopupData = {
      title: 'Player removed',
      content: [`Player <strong>${fname} ${lname}</strong> was successfully deleted.`]
    };
    const popupConfig = new PopupConfig(PopupThemesEnum.SUCCESS, popupData);
    this.dynamicComponentsService.appendPopup(PopupComponent, popupConfig, 7000);
  }

  private showPlayerRemovalErrorPopup(fname: string, lname: string): void {
    const popupData: PopupData = {
      title: 'Player removal error',
      content: [`Error during <strong>${fname} ${lname}</strong> player deleting`]
    };
    const popupConfig = new PopupConfig(PopupThemesEnum.ERROR, popupData);
    this.dynamicComponentsService.appendPopup(PopupComponent, popupConfig, 7000);
  }
}
