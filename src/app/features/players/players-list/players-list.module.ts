import { NgModule } from '@angular/core';
import { SharedModule } from "../../../shared/shared.module";

import { PlayersListComponent } from './players-list.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { PlayerFormDialogComponent } from "./player-form-dialog/player-form-dialog.component";
import { FormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatSelectModule } from "@angular/material/select";
import { MatDialogModule } from "@angular/material/dialog";
import { MatInputModule } from "@angular/material/input";
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [{path: '', component: PlayersListComponent}];

@NgModule({
  imports: [
    SharedModule,
    [RouterModule.forChild(routes)],
    MatExpansionModule,
    MatIconModule,
    FormsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatSelectModule,
    MatDialogModule,
    MatInputModule,
    MatMomentDateModule
  ],
  declarations: [
    PlayersListComponent,
    PlayerFormDialogComponent
  ],
  exports: [RouterModule]
})
export class PlayersListModule {}
