import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Player } from '../../../../models/player';
import { PostPlayerDto } from '../../../../models/dto/postPlayerDto';
import { DialogsService } from '../../../../core/services/dialogs.service';
import { NGXLogger } from 'ngx-logger';
import * as moment from 'moment';
import { NotificationDialogData } from "../../../../shared/dialogs/ok-dialog/notification-dialog.models";
import { PlayersService } from "../../players.service";
import { now } from "moment";

@Component({
  selector: 'bem-player-form-dialog',
  templateUrl: './player-form-dialog.component.html'
})
export class PlayerFormDialogComponent {

  private readonly DATE_FORMAT = 'YYYY-MM-DD';

  playerToSave = new PostPlayerDto();

  constructor(private playersService: PlayersService,
              public dialogRef: MatDialogRef<PlayerFormDialogComponent>,
              private dialogService: DialogsService,
              private logger: NGXLogger) {}

  createPlayer(): void {
    if (!this.isDateOfBirthValid(this.playerToSave.dateOfBirth)) {
      const data: NotificationDialogData = {
        title: 'Date of birth error',
        description: 'User is either too young or too old to be registered'
      }

      this.dialogService.openOkDialog(data, 'bem-error-dialog');
      return;
    }

    this.playerToSave.dateOfBirth = moment(this.playerToSave.dateOfBirth.toString()).format(this.DATE_FORMAT);

    this.playersService.createPlayer(this.playerToSave)
      .subscribe(
        (createdPlayer: Player) => {
          this.logger.info(`Player with id: ${createdPlayer.playerId} was successfully created`);
          this.close(createdPlayer);
        },

        (error => {
          const data: NotificationDialogData = {
            title: 'Saving error',
            description: 'Error during saving Player\'s info'
          }

          this.dialogService.openOkDialog(data, 'bem-error-dialog');
          this.logger.error(`Error during the new Player: ${error}`);
        })
      );
  }

  close(createdPlayer?: Player): void {
    this.dialogRef.close(createdPlayer);
  }

  private isDateOfBirthValid(dateOfBirth: string): boolean {
    const maxAge = moment(now()).subtract(100, "years");
    const minAge = moment(now()).subtract(10, "years");

    return moment(dateOfBirth, this.DATE_FORMAT).isAfter(maxAge) || moment(dateOfBirth).isBefore(minAge);
  }
}
